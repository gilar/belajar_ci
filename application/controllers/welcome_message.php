<! DOCTYPE html>
<html lang="en">
<head>
<title>Front-End Toko Online by Kursus-PHP.com</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php $this->load->view('layout/top_menu') ?>
<!-- Tampilkan semua product -->
<table class="table table-bordered table-striped table-hover">
<thead>
<tr>
<th>#</th>
<th>Product</th>
<th>Qty</th>
<th>Price</th>
<th>Subtotal</th>
</tr>
</thead>
<tbody>
$i=0;
<?php foreach ($this->cart->contents() as $items): ?>
$i++;
	<tr>
		<td><?= $i?></td>
		<td><?= $item['name']?></td>
		<td><?= $item['qty']?></td>
		<td><?= $item['price']?></td>
		<td><?= $item['subtotal']?></td>	
	</tr>
<?php endforeach;?> 
</body>
</html>
