<! DOCTYPE html>
<html lang="en">
<head>
<title>Front-End Toko Online by Kursus-PHP.com</title>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<?php $this->load->view('layout/top_menu') ?>
<!-- Tampilkan semua product -->
<?php if($history !=false) : ?>
<?= $this->session->flashdata('message')?>
<table class="table table-bordered table-striped table-hover">
<thead>
<tr>
		<th>Invoice ID#</th>
		<th>Invoice Date</th>
		<th>Due Date</th>
		<th>Total Amount</th>
		<th>Status</th>
</tr>
</thead>
<tbody>
<?php $i=0;foreach ($history as $row): $i++; ?>
	<tr>
		<td><?= $row->id?></td>
		<td><?= $row->date?></td>
		<td><?= $row->due_date?></td>
		<td><?= $row->total?></td>
		<td><?= $row->status?><?php if($row->status == 'unpaid'){ 
			anchor('customer/payment_confirmation/'
			.$row->id,'Confirm Payment',array('class'=>'btn btn-primary btn-xs'));
}
?>
<?php endforeach;?> 
</tbody>
</table>
<?php else : ?>
	<p align="center">There are no shopping history for you ... <?=anchor('/','Shop now') ?></p>
<?php endif; ?>
</body>

</html>
