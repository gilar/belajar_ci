<!DOCTYPE html>
<html>
<head>
	<title>Admin Page | View All Invoices Detail</title>
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
		
	<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.css">
	
</head>	 
<body>
<?php $this->load->view('backend/admin_menu') ?>
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10">
<h3>Item Ordered in Invoice #<?=$invoice->id?></h3> 
<table id="myTable" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
				<th>Product ID</th>
				<th>Product Name</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Subtotal</th>	
		</tr>
		</thead>
		<tbody>
	
		<?php 
		$total = 0;
		foreach ($orders as $order) :
		$subtotal = $order->qty * $order->price;
		$total += $subtotal;
		 ?>

	
		<tr>
		<td><?=$order->product_id?></td>
		<td><?=$order->product_name?></td>
		<td><?=$order->qty?></td>
		<td><?=$order->price?></td>
		<td><?=$subtotal?></td>

				
		</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="4" align="right">Total</td>
			<td><?=$total?></td>
		</tr>
	</tfoot>

		</tbody>
		
	<?php endforeach; ?>
	</tbody>

</table> 	

</div>
<div class="col-md-1"></div>

<script>
	$(document).ready(function(){
    $('#myTable').DataTable();
}); 
	</script>
</body>
</html>