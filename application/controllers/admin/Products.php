	<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	 public function __construct(){
	 	parent::__construct();

	 	if($this->session->userdata('group') !='1'){
	 		$this->session->set_flashdata('error','Sorry,you are not logged in!');
	 		redirect('login');
	 	}

	 	$this->load->model('Model_products');
	 }

	public function index()
	{
		$data['products'] = $this->Model_products->all();
		$this->load->view('backend/view_all_products',$data);
	}

	public function create(){

			    $this->form_validation->set_rules('name', 'Product Name', 'required');
                $this->form_validation->set_rules('description', 'Product Description', 'required');
                $this->form_validation->set_rules('price', 'Product Price', 'required|integer');
                $this->form_validation->set_rules('stock', 'Available Stock', 'required|integer');
         	   //    $this->form_validation->set_rules('userfile', 'Product Image', 'required');

                if ($this->form_validation->run() == FALSE)
                {

	    		$this->load->view('backend/form_tambah_product');
	}else{
				$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'jpg|png';
                $config['max_size']             = 3000;
                $config['max_width']            = 2000;
                $config['max_height']           = 2000;

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload())
                {
                	$this->load->view('backend/form_tambah_product');
	} 
	else{
                	//file berhasil diupload
                	//ekseskusi query insert
                	$gambar = $this->upload->data();
					$data_product = array('name'=>set_value('name'),
					'description'=>set_value('description'),
					'price'=>set_value('price'),
					'stock'=>set_value('stock'),
					'image'=>$gambar['file_name']);
					$this->Model_products->create($data_product);
					redirect('admin/products'); 
			
                }


				}  

}
	public function update($id){

	         $this->form_validation->set_rules('name', 'Product Name', 'required');
             $this->form_validation->set_rules('description', 'Product Description', 'required');
             $this->form_validation->set_rules('price', 'Product Price', 'required|integer');
             $this->form_validation->set_rules('stock', 'Available Stock', 'required|integer');
            // $this->form_validation->set_rules('userfile', 'Product Image', 'required');

 	
                if ($this->form_validation->run() == FALSE)
                { 
                $data['product'] = $this->Model_products->find($id);	
				$this->load->view('backend/form_edit_product',$data);
			}else{
						// form submit dengan gambar diisi

				//load uploading file library
				if($_FILES['userfile'] ['name'] !=''){			
				$config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'jpg|png';
                $config['max_size']             = 300;
                $config['max_width']            = 2000;
                $config['max_height']           = 2000;

                $this->load->library('upload', $config);
            
    	              
                if ( ! $this->upload->do_upload()){ 
               
                	//file gagal upload ->kembali ke form tambah
                	 $data['product'] = $this->Model_products->find($id);	
					$this->load->view('backend/form_edit_product',$data);
	 
                }else{
                	//file berhasil diupload
                	//ekseskusi query insert

                	$gambar = $this->upload->data();
					$data_product = array(
					'name'=>set_value('name'),
					'description'=>set_value('description'),
					'price'=>set_value('price'),
					'stock'=>set_value('stock'),
					'image'=>$gambar['file_name']);
					$this->Model_products->update($id,$data_product);
					redirect('admin/products'); 
			
                }

                }else {
                	//form submit dengan gambar dikosongkan
                	
					$data_product = array(
					'name'=>set_value('name'),
					'description'=>set_value('description'),
					'price'=>set_value('price'),
					'stock'=>set_value('stock')
					);
					$this->Model_products->update($id,$data_product);
					redirect('admin/products'); 
			
                }

				} 
 }

	
	public function delete($id){
		$this->Model_products->delete($id);
		redirect('admin/Products');

	}

		

	
		
}
